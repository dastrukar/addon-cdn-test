> ignore the title of this project, it doesn't actually use a cdn

## What?
This repository is just a place to store addon "entries" for use in a random project that I'm working on.

For the actual project please refer to [this](https://gitlab.com/dastrukar/hddons-list-test).


## Syntax for entries
The syntax for entries isn't really that hard.
It's just ~~JSON (JavaScript Object Notation)~~ YAML syntax and only has about 5 (or more?) variables to set.
Please do note that all variable names are **case sensitive**.

I suggest that you use template.yaml as it's more convinient that way.

Anyways, here's an example:
```YAML
title: "Title/Name of the addon goes here."
credits: "by Author name here."
flairs:
    - requirement: "Requires addon X. Leave empty if none."
description: "The description of the addon goes here."
flag: "A class to set for this addon's entry goes here. Leave empty if none."
links:
    # You can have more than one link. Note the hyphens
    custom:
        "Custom Link Label": "Link goes here."
    git:
        - "Link to your addon's git repository. Leave empty if none"
    forum:
        - "Link to ZDoom Forum about your addon. Leave empty if none"
    direct:
	    - "Link to directly download your addon. Leave empty if none"
```

Note: Only `title`, `credits`, and `description` are required for the entry to display properly.

Which means you can have an entry that looks like this:
```JSON
title: "Title/Name"
credits: "by Author"
description: "Lorem Ipsum."
```
Though, maybe don't exclude `links`. I mean, how else are people going to download your addon?

Side note: It doesn't really matter as to how you order the variables, the only thing that matters is that you include them and set a value to them.

## What's GitLab CI doing?
Every time a commit is pushed, GitLab CI will proceed to get all the JSON files in any subdirectory (currently it only grabs from [`weapons`](/weapons)) and put the contents into a JSON file named `<SUBDIRECTORY>_list.json`.

This list is then used by the [website](https://dastrukar.gitlab.io/addons-list-test) to create addon entries.

Note: All addon entries are sorted by their filename, so make sure the filename is the same as the addon's name. (or as close as you can get to the addon's name)

### The old method
Originally, GitLab CI created a `list.json` in the subdirectory it was grabbing from, and the website would then read from that file to create direct raw links for accessing the other JSON files.

Obviously, this isn't a very good idea, because [GitLab's API has a request limit](https://docs.gitlab.com/ee/user/gitlab_com/index.html#gitlabcom-specific-rate-limits).
