import os
import sys
import json
from ruamel.yaml import YAML
from datetime import datetime

# Main

if len(sys.argv) > 2:
	print("You provided more than 1 argument. Don't do that. Ignoring the other arguments.")
elif len(sys.argv) < 2:
	print("Please provide subdirectory to read from.")
	print("Syntax: compiler.py <subdirectory>")
	exit()

# Get files
subdirectory = sys.argv[1]
files = [subdirectory + "/" + file for file in os.listdir(subdirectory) if os.path.isfile(os.path.join(subdirectory, file))]
files.sort()
print(str(files))

# Get files contents
first = True

date = datetime.now().strftime("%m/%d/%Y")
result = '{"last_updated":"' + date + '",'
result += '"list":{'
for f in files:
	print("Reading " + f)
	if not first:
		result += ','
	result += '"' + f + '":'

	with open(f, "r") as file:
		contents = YAML(typ="safe").load(file.read())
		result += json.dumps(contents)
		first = False

result += '}}'

# Wait a minute... Isn't this just JSON format?
# Yes. Now export it for the website to read :]
with open(subdirectory + "_list.json", "w") as l:
	l.write(result)
	print("Wrote to " + subdirectory + "_list.json")
